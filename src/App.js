import React from "react";
import { createStore } from "redux";
import "./App.css";

function counter(state = 0, action) {
  switch (action.type) {
    case "INC":
      return state + 1;
    case "DEC":
      return state - 1;
    default:
      return state;
  }
}

const store = createStore(counter);

store.subscribe(() => console.log(store.getState()));

store.dispatch({ type: "INC" });
store.dispatch({ type: "INC" });

store.dispatch({ type: "DEC" });
store.dispatch({ type: "DEC" });

function App() {
  return <div>Count = {store.getState()}</div>;
}

export default App;
